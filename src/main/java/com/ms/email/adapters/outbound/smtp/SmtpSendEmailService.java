package com.ms.email.adapters.outbound.smtp;

import com.ms.email.application.domains.Email;
import com.ms.email.application.ports.SendEmailServicePort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class SmtpSendEmailService implements SendEmailServicePort {

    @Autowired
    private JavaMailSender emailSender;

    public void sendEmailSmtp(Email email) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(email.getEmailRemetente());
        message.setTo(email.getEmailDestinatario());
        message.setSubject(email.getTitulo());
        message.setText(email.getCorpo());

        emailSender.send(message);
    }
}