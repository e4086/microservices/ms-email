package com.ms.email.adapters.outbound.persistence;

import com.ms.email.adapters.outbound.persistence.entities.EmailEntity;
import com.ms.email.application.domains.Email;
import com.ms.email.application.domains.PageInfo;
import com.ms.email.application.ports.EmailRepositoyPort;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
@Primary
public class PostgresEmailRepository implements EmailRepositoyPort {

    private final SpringDataPostgresEmailRepository emailRepository;

    public PostgresEmailRepository(SpringDataPostgresEmailRepository emailRepository) {
        this.emailRepository = emailRepository;
    }

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public Email save(Email email) {
        EmailEntity emailEntity = emailRepository.save(modelMapper.map(email, EmailEntity.class));
        return modelMapper.map(emailEntity, Email.class);
    }

    @Override
    public List<Email> findAll(PageInfo pageInfo) {
        Pageable pageable = PageRequest.of(pageInfo.getPageNumber(), pageInfo.getPageSize());
        return emailRepository.findAll(pageable).stream()
                .map(emailEntity -> modelMapper.map(emailEntity, Email.class))
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Email> buscarPorId(UUID id) {
        Optional<EmailEntity> optionalEmailEntity = emailRepository.findById(id);

        if (optionalEmailEntity.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.of(modelMapper.map(optionalEmailEntity.get(), Email.class));
        }
    }
}
