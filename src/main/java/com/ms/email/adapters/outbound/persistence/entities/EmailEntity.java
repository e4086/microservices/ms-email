package com.ms.email.adapters.outbound.persistence.entities;

import com.ms.email.application.domains.enums.EStatusEmail;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Entity
@Table(name = "tb_email")
public class EmailEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "em_id", nullable = false)
    private UUID id;

    @Column(name = "em_usuario", nullable = false)
    private String usuarioDestinatario;

    @Column(name = "em_email_from", nullable = false)
    private String emailRemetente;

    @Column(name = "em_email_to", nullable = false)
    private String emailDestinatario;

    @Column(name = "em_titulo", nullable = false)
    private String titulo;

    @Column(name = "em_corpo", columnDefinition = "TEXT")
    private String corpo;

    @Column(name = "em_data_envio")
    private LocalDateTime dataEnvio;

    @Column(name = "em_status")
    private EStatusEmail status;
}
