package com.ms.email.adapters.configuration;

import com.ms.email.EmailApplication;
import com.ms.email.application.ports.EmailRepositoyPort;
import com.ms.email.application.ports.SendEmailServicePort;
import com.ms.email.application.services.EmailServiceImpl;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = EmailApplication.class)
public class BeanConfiguration {

    @Bean
    public EmailServiceImpl emailServiceImpl(EmailRepositoyPort emailRepositoyPort, SendEmailServicePort sendEmailServicePort) {
        return new EmailServiceImpl(emailRepositoyPort, sendEmailServicePort);
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

}
