package com.ms.email.adapters.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class DatabaseConfig {

    @Value("${database.config.url}")
    private String url;

    @Value("${database.config.username}")
    private String usuario;

    @Value("${database.config.password}")
    private String senha;

    @Bean
    public DataSource getDatasource() {
        return DataSourceBuilder.create()
                .url(url)
                .username(usuario)
                .password(senha)
                .build();
    }

}
