package com.ms.email.adapters.dtos;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
public class EmailDto {

    @NotBlank
    private String usuarioDestinatario;

    @NotBlank
    @Email
    private String emailRemetente;

    @NotBlank
    @Email
    private String emailDestinatario;

    @NotBlank
    private String titulo;

    @NotBlank
    private String corpo;
}
