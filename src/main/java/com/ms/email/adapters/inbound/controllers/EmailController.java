package com.ms.email.adapters.inbound.controllers;

import com.ms.email.adapters.dtos.EmailDto;
import com.ms.email.application.domains.Email;
import com.ms.email.application.domains.PageInfo;
import com.ms.email.application.ports.EmailServicePort;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.hateoas.server.core.DummyInvocationUtils.methodOn;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
public class EmailController {

    @Autowired
    private EmailServicePort emailServicePort;

    @PostMapping("/enviarEmail")
    public ResponseEntity<Email> enviarEmail(@RequestBody @Valid EmailDto emailDto) {
        var emailModel = new Email();

        BeanUtils.copyProperties(emailDto, emailModel);

        emailServicePort.enviarEmail(emailModel);

        return new ResponseEntity<>(emailModel, HttpStatus.CREATED);
    }

    @GetMapping("/emails")
    public ResponseEntity<Page<Email>> buscarTodos(@PageableDefault(size = 5, sort = "id", direction = Sort.Direction.DESC) Pageable pageable) {
        var pageInfo = new PageInfo();
        BeanUtils.copyProperties(pageable, pageInfo);
        List<Email> emails = emailServicePort.buscarTodos(pageInfo);

        emails.forEach(email -> email.add(linkTo(methodOn(EmailController.class).buscarPorId(email.getId())).withSelfRel()));

        return new ResponseEntity<>(new PageImpl<>(emails, pageable, emails.size()), HttpStatus.OK);
    }

    @GetMapping("/emails/{id}")
    public ResponseEntity<Object> buscarPorId(@PathVariable("id") UUID id) {
        Optional<Email> emailOptional = emailServicePort.buscarPorId(id);

        if (emailOptional.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Email inexistente.");
        } else {
            emailOptional.get().add(linkTo(methodOn(EmailController.class).buscarTodos(PageRequest.of(0,1))).withRel("Lista de e-mails"));

            return ResponseEntity.status(HttpStatus.OK).body(emailOptional.get());
        }
    }
}
