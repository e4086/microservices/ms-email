package com.ms.email.adapters.inbound.consumers;

import com.ms.email.adapters.dtos.EmailDto;
import com.ms.email.application.domains.Email;
import com.ms.email.application.ports.EmailServicePort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class EmailConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailConsumer.class);

    @Autowired
    private EmailServicePort emailServicePort;

    @RabbitListener(queues = "${spring.rabbitmq.queue}")
    public void listen(@Payload EmailDto emailDto) {
        var emailModel = new Email();

        BeanUtils.copyProperties(emailDto, emailModel);

        emailServicePort.enviarEmail(emailModel);

        LOGGER.info("Status Email: " + emailModel.getStatus().toString());
    }
}
