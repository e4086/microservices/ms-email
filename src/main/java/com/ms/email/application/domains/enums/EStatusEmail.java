package com.ms.email.application.domains.enums;

public enum EStatusEmail {
    ENVIADO("Enviado"),
    ERRO("Erro");

    private final String value;

    EStatusEmail(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
