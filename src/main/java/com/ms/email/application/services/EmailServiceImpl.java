package com.ms.email.application.services;

import com.ms.email.application.domains.Email;
import com.ms.email.application.domains.PageInfo;
import com.ms.email.application.domains.enums.EStatusEmail;
import com.ms.email.application.ports.EmailRepositoyPort;
import com.ms.email.application.ports.EmailServicePort;
import com.ms.email.application.ports.SendEmailServicePort;
import org.springframework.mail.MailException;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class EmailServiceImpl implements EmailServicePort {

    private final EmailRepositoyPort emailRepositoyPort;

    private final SendEmailServicePort sendEmailServicePort;

    public EmailServiceImpl(final EmailRepositoyPort emailRepositoyPort, final SendEmailServicePort sendEmailServicePort) {
        this.emailRepositoyPort = emailRepositoyPort;
        this.sendEmailServicePort = sendEmailServicePort;
    }

    public Email enviarEmail(Email email) {
        email.setDataEnvio(LocalDateTime.now());

        try {
            sendEmailServicePort.sendEmailSmtp(email);
            email.setStatus(EStatusEmail.ENVIADO);
        } catch (MailException exception) {
            email.setStatus(EStatusEmail.ERRO);
        } finally {
            return salvar(email);
        }
    }

    @Override
    public Email salvar(Email email) {
        return emailRepositoyPort.save(email);
    }

    @Override
    public List<Email> buscarTodos(PageInfo pageInfo) {
        return emailRepositoyPort.findAll(pageInfo);
    }

    @Override
    public Optional<Email> buscarPorId(UUID id) {
        return emailRepositoyPort.buscarPorId(id);
    }
}
