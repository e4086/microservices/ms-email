package com.ms.email.application.ports;

import com.ms.email.application.domains.Email;
import com.ms.email.application.domains.PageInfo;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface EmailRepositoyPort {
    Email save(Email email);

    List<Email> findAll(PageInfo pageInfo);

    Optional<Email> buscarPorId(UUID id);

}
