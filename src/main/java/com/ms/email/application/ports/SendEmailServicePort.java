package com.ms.email.application.ports;

import com.ms.email.application.domains.Email;

public interface SendEmailServicePort {
    void sendEmailSmtp(Email email);
}
