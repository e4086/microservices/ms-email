package com.ms.email.application.ports;

import com.ms.email.application.domains.Email;
import com.ms.email.application.domains.PageInfo;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface EmailServicePort {
    Email enviarEmail(Email email);

    List<Email> buscarTodos(PageInfo pageInfo);

    Optional<Email> buscarPorId(UUID id);

    Email salvar(Email email);
}
